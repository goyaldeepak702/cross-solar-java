package com.crossover.techtrial.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.crossover.techtrial.dto.DailyElectricity;
import com.crossover.techtrial.model.HourlyElectricity;

/**
 * HourlyElectricity Repository is for all operations for HourlyElectricity.
 * @author Crossover
 */
@RestResource(exported = false)
public interface HourlyElectricityRepository 
    extends PagingAndSortingRepository<HourlyElectricity,Long> {
	
  @Query("SELECT new com.crossover.techtrial.dto.DailyElectricity( "
	        + "MIN(e.readingAt) as date, "
	        + "SUM(e.generatedElectricity) as sum, "
	        + "AVG(e.generatedElectricity) as average, "
	        + "MIN(e.generatedElectricity) as min, "
	        + "MAX(e.generatedElectricity) as max) "
	       + "FROM HourlyElectricity e "
	     + " WHERE e.readingAt IN ( "
	     + "     SELECT DISTINCT e2.readingAt "
	     + "       FROM HourlyElectricity e2, Panel p " 
	     + "      WHERE e2.panel.id = p.id )"
	     + "  AND e.panel.serial = ?1 "
       + "  AND e.readingAt < ?2 "	     
	     + "GROUP BY e.readingAt, e.panel.id ")
	List<DailyElectricity> getAllDailyElectricity(String panelSerial, LocalDateTime today);

	Page<HourlyElectricity> findAllByPanelIdOrderByReadingAtDesc(Long panelId,Pageable pageable);
}
