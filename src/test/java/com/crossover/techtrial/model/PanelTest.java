package com.crossover.techtrial.model;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * PanelTest class will test all APIs in Panel.java.
 * 
 * @author Crossover
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PanelTest {
  
  @Test
  public void testEqualsAndHashCode() {
    Panel panel1 = new Panel("1111", 111.11, 111.11, "aaaaa");
    Panel panel2 = new Panel("1111", 111.11, 111.11, "aaaaa");
    assertTrue(panel1.equals(panel2) && panel2.equals(panel1));
    assertTrue(panel1.hashCode() == panel2.hashCode());
  }
}
